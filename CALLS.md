## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for NS1 Enterprise. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for NS1 Enterprise.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the NS1 Enterprise. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postOpsBootstrap(body, callback)</td>
    <td style="padding:15px">Bootstrap the first operator user</td>
    <td style="padding:15px">{base_path}/{version}/ops/bootstrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsOperators(callback)</td>
    <td style="padding:15px">View all operator users</td>
    <td style="padding:15px">{base_path}/{version}/ops/operators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOpsOperators(body, callback)</td>
    <td style="padding:15px">Create a new operator user</td>
    <td style="padding:15px">{base_path}/{version}/ops/operators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsOperatorsOperatorId(operatorId, callback)</td>
    <td style="padding:15px">View operator details</td>
    <td style="padding:15px">{base_path}/{version}/ops/operators/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOpsOperatorsOperatorId(operatorId, body, callback)</td>
    <td style="padding:15px">Modify details of an operator</td>
    <td style="padding:15px">{base_path}/{version}/ops/operators/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpsOperatorsOperatorId(operatorId, callback)</td>
    <td style="padding:15px">Delete an operator</td>
    <td style="padding:15px">{base_path}/{version}/ops/operators/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsOrgs(callback)</td>
    <td style="padding:15px">View a list of organizations</td>
    <td style="padding:15px">{base_path}/{version}/ops/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOpsOrgs(body, callback)</td>
    <td style="padding:15px">Create an organization</td>
    <td style="padding:15px">{base_path}/{version}/ops/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsOrgsOrgid(orgid, callback)</td>
    <td style="padding:15px">View organization details</td>
    <td style="padding:15px">{base_path}/{version}/ops/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOpsOrgsOrgid(orgid, body, callback)</td>
    <td style="padding:15px">Modify an organization</td>
    <td style="padding:15px">{base_path}/{version}/ops/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpsOrgsOrgid(orgid, token, callback)</td>
    <td style="padding:15px">Delete an organization</td>
    <td style="padding:15px">{base_path}/{version}/ops/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsOrgsOrgidServiceGroups(orgid, callback)</td>
    <td style="padding:15px">View service groups associated with an organization</td>
    <td style="padding:15px">{base_path}/{version}/ops/orgs/{pathv1}/service/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsServiceDefs(callback)</td>
    <td style="padding:15px">View list of service definitions</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/defs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOpsServiceDefs(body, callback)</td>
    <td style="padding:15px">Create a new service definition</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/defs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsServiceDefsDefinitionid(definitionid, callback)</td>
    <td style="padding:15px">View service definition details</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/defs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOpsServiceDefsDefinitionid(definitionid, body, callback)</td>
    <td style="padding:15px">Modify a service definition</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/defs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpsServiceDefsDefinitionid(definitionid, callback)</td>
    <td style="padding:15px">Delete a service definition</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/defs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsServiceGroups(callback)</td>
    <td style="padding:15px">View a list of service groups</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOpsServiceGroups(body, callback)</td>
    <td style="padding:15px">Create a service group</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsServiceGroupsServicegroupid(servicegroupid, callback)</td>
    <td style="padding:15px">View service group details</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOpsServiceGroupsServicegroupid(servicegroupid, callback)</td>
    <td style="padding:15px">Modify a service group</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpsServiceGroupsServicegroupid(servicegroupid, callback)</td>
    <td style="padding:15px">Delete a service group</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOpsServiceGroupsServicegroupidOrgOrgid(servicegroupid, orgid, callback)</td>
    <td style="padding:15px">Associate with an organization</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups/{pathv1}/org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpsServiceGroupsServicegroupidOrgs(servicegroupid, callback)</td>
    <td style="padding:15px">View organizations associated with a service group</td>
    <td style="padding:15px">{base_path}/{version}/ops/service/groups/{pathv1}/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetwork(callback)</td>
    <td style="padding:15px">View a list of networks</td>
    <td style="padding:15px">{base_path}/{version}/ipam/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamNetwork(body, callback)</td>
    <td style="padding:15px">Create a network</td>
    <td style="padding:15px">{base_path}/{version}/ipam/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetworkNetworkId(networkId, callback)</td>
    <td style="padding:15px">View a network</td>
    <td style="padding:15px">{base_path}/{version}/ipam/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamNetworkNetworkId(networkId, body, callback)</td>
    <td style="padding:15px">Update a network</td>
    <td style="padding:15px">{base_path}/{version}/ipam/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamNetworkNetworkId(networkId, callback)</td>
    <td style="padding:15px">Remove a network</td>
    <td style="padding:15px">{base_path}/{version}/ipam/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetworkNetworkIdReport(networkId, callback)</td>
    <td style="padding:15px">View a network report</td>
    <td style="padding:15px">{base_path}/{version}/ipam/network/{pathv1}/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddress(callback)</td>
    <td style="padding:15px">View a list of root addresses</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamAddress(body, callback)</td>
    <td style="padding:15px">Create a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressSearch(network, max, status, tag, orderBy, ascDesc, mask, prefix, callback)</td>
    <td style="padding:15px">Search for an address object</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressAddressId(addressId, callback)</td>
    <td style="padding:15px">View a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamAddressAddressId(addressId, body, callback)</td>
    <td style="padding:15px">Edit a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAddressAddressId(addressId, callback)</td>
    <td style="padding:15px">Delete a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamAddressAddressIdSplit(addressId, body, callback)</td>
    <td style="padding:15px">Split a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamAddressAddressIdMerge(addressId, body, callback)</td>
    <td style="padding:15px">Merge a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressAddressIdChildren(addressId, callback)</td>
    <td style="padding:15px">View address children</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressAddressIdParent(addressId, callback)</td>
    <td style="padding:15px">View address parent</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/parent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressAddressIdAdjacent(addressId, previous, callback)</td>
    <td style="padding:15px">View next (or previous) available prefix</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/adjacent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressSubnetIdPool(subnetId, callback)</td>
    <td style="padding:15px">View a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamAddressSubnetIdPool(subnetId, body, callback)</td>
    <td style="padding:15px">Create a pool (IP range) within a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressSubnetIdPoolPoolId(subnetId, poolId, callback)</td>
    <td style="padding:15px">View a pool (IP range) within a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/pool/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamAddressSubnetIdPoolPoolId(subnetId, poolId, body, callback)</td>
    <td style="padding:15px">Update a pool (IP range) within a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/pool/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAddressSubnetIdPoolPoolId(subnetId, poolId, callback)</td>
    <td style="padding:15px">Delete a pool (IP range) within a subnet</td>
    <td style="padding:15px">{base_path}/{version}/ipam/address/{pathv1}/pool/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamTags(callback)</td>
    <td style="padding:15px">View all tags</td>
    <td style="padding:15px">{base_path}/{version}/ipam/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamKeys(callback)</td>
    <td style="padding:15px">View all organization keys</td>
    <td style="padding:15px">{base_path}/{version}/ipam/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScope(callback)</td>
    <td style="padding:15px">Generate a list of the active scopes</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpScope(body, callback)</td>
    <td style="padding:15px">Create a scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopeScopeID(scopeID, callback)</td>
    <td style="padding:15px">View scope details</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpScopeScopeID(scopeID, body, callback)</td>
    <td style="padding:15px">Modify a scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpScopeScopeID(scopeID, callback)</td>
    <td style="padding:15px">Remove a scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopeGroup(callback)</td>
    <td style="padding:15px">List scope groups</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpScopeGroup(body, callback)</td>
    <td style="padding:15px">Create a scope group</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scope/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopegroupScopegroupID(scopegroupID, callback)</td>
    <td style="padding:15px">View scope group</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scopegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpScopegroupScopegroupID(scopegroupID, body, callback)</td>
    <td style="padding:15px">Update scope group by ID</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scopegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpScopegroupScopegroupID(scopegroupID, callback)</td>
    <td style="padding:15px">Remove scope group by ID</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scopegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopegroupScopegroupIDReport(scopegroupID, callback)</td>
    <td style="padding:15px">Returns a report about the specified scope group.</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/scopegroup/{pathv1}/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpReservation(callback)</td>
    <td style="padding:15px">List reservations</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/reservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpReservation(body, callback)</td>
    <td style="padding:15px">Create a reservation</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/reservation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpReservationReservationId(reservationId, callback)</td>
    <td style="padding:15px">View a reservation's details</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/reservation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpReservationReservationId(reservationId, body, callback)</td>
    <td style="padding:15px">Modify a reservation</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/reservation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpReservationReservationId(reservationId, callback)</td>
    <td style="padding:15px">Delete a reservation</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/reservation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLease(callback)</td>
    <td style="padding:15px">List leases</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpOptiondef(callback)</td>
    <td style="padding:15px">List option definitions</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/optiondef?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDhcpOptiondefSpaceKey(space, key, body, callback)</td>
    <td style="padding:15px">Modify an custom DHCP option definition</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/optiondef/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDhcpOptiondefSpaceKey(space, key, body, callback)</td>
    <td style="padding:15px">Create an custom DHCP option definition</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/optiondef/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpOptiondefSpaceKey(space, key, callback)</td>
    <td style="padding:15px">Delete an option definition</td>
    <td style="padding:15px">{base_path}/{version}/dhcp/optiondef/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActivedirectory(callback)</td>
    <td style="padding:15px">View Active Directory configuration</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActivedirectory(body, callback)</td>
    <td style="padding:15px">Modify Active Directory configuration</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putActivedirectory(body, callback)</td>
    <td style="padding:15px">Create an Active Directory configuration</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActivedirectory(callback)</td>
    <td style="padding:15px">Delete Active Directory configuration</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActivedirectoryTest(body, callback)</td>
    <td style="padding:15px">Test the Active Directory connection</td>
    <td style="padding:15px">{base_path}/{version}/activedirectory/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
