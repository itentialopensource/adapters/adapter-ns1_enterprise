# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Ns1_enterprise System. The API that was used to build the adapter for Ns1_enterprise is usually available in the report directory of this adapter. The adapter utilizes the Ns1_enterprise API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The NS1 Enterprise adapter from Itential is used to integrate the Itential Automation Platform (IAP) with IBM NS1 Enterprise DDI to offer a DNS, DHCP and IPAM solution for configuration and orchestration. 

With this adapter you have the ability to perform operations with NS1 Enterprise such as:

- IPAM
- DHCP

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
